class ShopModel {
  final int id;
  final String label;
  final int rating;
  final int review;
  final String imageUrl;
  final String phoneNumber;
  final String price;
  final String type;
  final String city;
  final int view;
  final double star;
  final String location;

  ShopModel(
      {this.id,
      this.label,
      this.star,
      this.rating,
      this.review,
      this.imageUrl,
      this.phoneNumber,
      this.price,
      this.type,
      this.view,
      this.city,
      this.location});
}
