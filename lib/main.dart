import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jong_nhams/cumtom_bottom_app_bar/custom_bottom_app_bar.dart';
import 'package:jong_nhams/cumtom_bottom_app_bar/custom_bottom_app_bar_item.dart';
import 'package:jong_nhams/pages/discount_coupon_page.dart';
import 'package:jong_nhams/pages/favorite_page.dart';
import 'package:jong_nhams/pages/home_page.dart';
import 'package:jong_nhams/pages/my_account.dart';

void main() => runApp(JongNhams());

class JongNhams extends StatefulWidget {
  @override
  _JongNhamsState createState() => _JongNhamsState();
}

class _JongNhamsState extends State<JongNhams> {
  int pageIndex = 0;

  List<Widget> pages = [
    HomePage(key: UniqueKey(), title: 'Home'),
    DiscountCouponPage(
      key: UniqueKey(),
      title: 'Discount Coupon',
    ),
    FavoritePage(key: UniqueKey(), title: 'Favorite'),
    MyAccount(key: UniqueKey(), title: 'Me')
  ];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.red, primaryColor: Color(0XFFbd1e1f)),
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: pages[pageIndex],
        bottomNavigationBar: BottomAppBar(
          child: CustomBottomAppBar(
              onTab: (index) {
                setState(() {
                  pageIndex = index;
                });
                print('Index in main: $pageIndex');
              },
              items: [
                CustomBottomAppBarItem(
                    key: UniqueKey(), icon: Icons.home, label: 'Home'),
                CustomBottomAppBarItem(
                    key: UniqueKey(),
                    icon: Icons.confirmation_number,
                    label: 'Coupon'),
                CustomBottomAppBarItem(
                    key: UniqueKey(), icon: Icons.favorite, label: 'Favorite'),
                CustomBottomAppBarItem(
                    key: UniqueKey(), icon: Icons.person, label: 'Me'),
              ]),
        ),
      ),
    );
  }
}
