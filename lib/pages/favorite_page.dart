import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FavoritePage extends StatefulWidget {
  final String title;

  FavoritePage({Key key, this.title}) : super(key: key);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              FontAwesomeIcons.heart,
              size: 100.0,
              color: Colors.red,
            ),
            SizedBox(height: 50.0,),
            Text('Your favorite place')
          ],
        ),
      ),
    );
  }
}
