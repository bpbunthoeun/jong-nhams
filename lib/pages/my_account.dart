import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyAccount extends StatefulWidget {
  final String title;

  MyAccount({Key key, this.title}) : super(key: key);

  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              FontAwesomeIcons.user,
              size: 100.0,
              color: Colors.red,
            ),
            SizedBox(
              height: 50.0,
            ),
            Text('Welcome to my account')
          ],
        ),
      ),
    );
  }
}
