import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'detail_page/search_page.dart';

class HomePage extends StatefulWidget {
  final String title;

  HomePage({Key key, @required this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //todo put logo on first area on top of image
  List<Widget> pageView = [
    Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/coupon_01.png'),
              fit: BoxFit.cover)),
    ),
    Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/coupon_02.png'),
              fit: BoxFit.cover)),
    ),
    Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/coupon_03.png'),
              fit: BoxFit.cover)),
    ),
    Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/coupon_04.png'),
              fit: BoxFit.cover)),
    ),
    Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/coupon_05.png'),
              fit: BoxFit.cover)),
    ),
    Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/coupon_06.png'),
          fit: BoxFit.cover,
        ),
      ),
    )
  ];

  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0, keepPage: false);
    super.initState();
  }

  Widget _buildReco(
      {
      String imUrl,
      Function onTab,
      IconData icon,
      String text}) {
    return Expanded(
        child: Container(
      padding: EdgeInsets.only(left: 6.0, right: 6.0, top: 8.0, bottom: 12.0),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () => onTab,
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.4), BlendMode.darken),
              image: AssetImage(imUrl),
              fit: BoxFit.cover,
            )),
            child: Column(
              //mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    icon,
                    color: Colors.white,
                    size: 45.0,
                  ),
                  SizedBox(height: 10.0,),
                  Text(
                    text,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.normal),
                  )
                ]),
          ),
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Container(
            height: _height / 3,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        colorFilter: ColorFilter.mode(
                            Colors.black.withOpacity(0.4), BlendMode.darken),
                        image: AssetImage('assets/images/home_food_01.jpg'),
                        fit: BoxFit.cover),
                  ),
                  child: Center(
                    child: Image.asset(
                      'assets/images/logo.png',
                      //color: Colors.white,
                      //colorBlendMode: BlendMode.multiply,
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        clipBehavior: Clip.antiAlias,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (index) => SearchPage()
                                    //ShopDetailPage()
                                    ));
                          },
                          child: Container(
                            height: 50.0,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 8.0),
                                  child: Icon(
                                    FontAwesomeIcons.search,
                                    size: 22.0,
                                    color: Color(0XFFbd1e1f),
                                  ),
                                ),
                                Container(
                                  child: Text('Search restaurants',style: TextStyle(fontSize: 16.0,color: Colors.grey),),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Material(
            elevation: 5.0,
            child: Container(
              width: _width,
              height: 70.0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.gps_fixed,
                              color: Color(0XFFbd1e1f),
                            ),
                            SizedBox(
                              height: 3.0,
                            ),
                            Text('Near Me')
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 2.0),
                      width: 0.7,
                      color: Colors.grey,
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.fastfood,
                              color: Color(0XFFbd1e1f),
                            ),
                            SizedBox(
                              height: 3.0,
                            ),
                            Text('Type of food')
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 2.0),
                      width: 0.7,
                      color: Colors.grey,
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              Icons.pin_drop,
                              color: Color(0XFFbd1e1f),
                            ),
                            SizedBox(
                              height: 3.0,
                            ),
                            Text('Location')
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
              child: Text(
                'Recommended',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ),
          ),

          //todo build function for display the item as list
          Container(
              height: _height / 4.6,
              width: _width,
              child: Row(
                children: <Widget>[
                  _buildReco(
                      onTab: () {},
                      imUrl: 'assets/images/home_food_01.jpg',
                      icon: Icons.stars,
                      text: 'Most View'),
                  _buildReco(
                      onTab: () {},
                      imUrl: 'assets/images/home_food_02.jpg',
                      icon: FontAwesomeIcons.ticketAlt,
                      text: 'Most Used Coupon'),
                  _buildReco(
                      onTab: () {},
                      imUrl: 'assets/images/home_food_02.jpg',
                      icon: Icons.message,
                      text: 'Top Review'),
                ],
              )),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
              child: Text(
                'Coupon',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ),
          ),

          //todo create function to display page view and implement auto play
          //todo add tab in detail page
          Expanded(
              child: Container(
            child: PageView.builder(
                scrollDirection: Axis.horizontal,
                controller: _pageController,
                itemCount: pageView.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        child: pageView[index]),
                  );
                }),
          ))
        ],
      ),
    );
  }
}
