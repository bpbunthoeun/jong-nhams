import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DiscountCouponPage extends StatefulWidget {
  final String title;

  DiscountCouponPage({Key key, this.title}) : super(key: key);

  @override
  _DiscountCouponPageState createState() => _DiscountCouponPageState();
}

class _DiscountCouponPageState extends State<DiscountCouponPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              FontAwesomeIcons.percent,
              size: 100.0,
              color: Colors.red,
            ),
            SizedBox(
              height: 50.0,
            ),
            Text('Your favorite place')
          ],
        ),
      ),
    );
  }
}
