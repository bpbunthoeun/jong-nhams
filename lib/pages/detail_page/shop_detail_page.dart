import 'package:flutter/material.dart';
import 'package:jong_nhams/cumtom_bottom_app_bar/custom_bottom_app_bar.dart';
import 'package:jong_nhams/cumtom_bottom_app_bar/custom_bottom_app_bar_item.dart';
import 'package:jong_nhams/model/shop_model.dart';
import 'package:url_launcher/url_launcher.dart';

class ShopDetailPage extends StatefulWidget {
  final ShopModel shop;

  ShopDetailPage({this.shop});

  @override
  _ShopDetailPageState createState() => _ShopDetailPageState();
}

class _ShopDetailPageState extends State<ShopDetailPage>
    with TickerProviderStateMixin {
  ScrollController _scrollController;
  bool isCollapse = false;
  TabController _tabController;
  TabBar _tab;
  int currentTabIndex = 0;

  _launchURL() async {
    const url = 'tel:096 662 2227';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _mapRout(String location) async {
    //var = 'https://www.google.com/maps/search/11.5399083,104.9102096/@11.578667,104.8804897,15z',
    var url = 'https://www.google.com/maps/search/$location/@11.578667,104.8804897,15z';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }




  _scrollListener() {
    if (_scrollController.offset >= 0) {
      setState(() {
        isCollapse = true;
        print('reach the bottom');
      });
    }
    if (_scrollController.offset <= 0) {
      setState(() {
        isCollapse = false;
        print('reach the top');
      });
    }
  }

  //add listener for selected and unselected
  void _tabListener() {
    setState(() {
      currentTabIndex = _tabController.index;
      print('current index in tab listener: $currentTabIndex');
    });
  }

  @override
  void initState() {
    _tabController =
        TabController(initialIndex: currentTabIndex, length: 4, vsync: this)
          ..addListener(_tabListener);
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    _tab = TabBar(
      controller: _tabController,
      tabs: <Widget>[
        Container(
          height: _height,
          width: _width,
          child: Icon(
            Icons.store,
            size: 30,
            color: currentTabIndex == 0 ? Colors.red : Colors.grey,
          ),
        ),
        Container(
          child: Icon(
            Icons.fastfood,
            size: 30,
            color: currentTabIndex == 1 ? Colors.red : Colors.grey,
          ),
        ),
        Container(
          child: Icon(
            Icons.calendar_today,
            size: 30,
            color: currentTabIndex == 2 ? Colors.red : Colors.grey,
          ),
        ),
        Container(
          child: Icon(
            Icons.message,
            size: 30,
            color: currentTabIndex == 3 ? Colors.red : Colors.grey,
          ),
        ),
      ],
    );

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context, innerBoxIsScroll) {
          return <Widget>[
            SliverAppBar(
                floating: true,
                pinned: true,
                expandedHeight: _height / 3,
                actions: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 2.0),
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: isCollapse
                          ? BoxDecoration()
                          : BoxDecoration(
                              shape: BoxShape.circle, color: Colors.grey.withOpacity(0.7)),
                      child: IconButton(
                          icon: Icon(Icons.favorite_border), onPressed: () {}),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 2.0, left: 2.0),
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: isCollapse
                          ? BoxDecoration()
                          : BoxDecoration(
                              shape: BoxShape.circle, color: Colors.grey.withOpacity(0.7)),
                      child:
                          IconButton(icon: Icon(Icons.edit), onPressed: () {}),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 4.0, left: 2.0),
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: isCollapse
                          ? BoxDecoration()
                          : BoxDecoration(
                              shape: BoxShape.circle, color: Colors.grey.withOpacity(0.7)),
                      child:
                          IconButton(icon: Icon(Icons.share), onPressed: () {}),
                    ),
                  )
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: Stack(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    '${widget.shop.imageUrl}'),
                                fit: BoxFit.cover)),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.only(
                              bottom: 8.0, right: 8.0, left: 8.0),
                          height: (_height / 3) / 2.8,
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 2.0),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          '${widget.shop.label}',
                                          style: TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.black87,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.yellow
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.yellow
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.yellow
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.yellow
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.yellow
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: 15.0,
                                                  color: Colors.grey
                                                      .withOpacity(0.8),
                                                ),
                                                SizedBox(
                                                  width: 2.0,
                                                ),
                                                Text(
                                                  '${widget.shop.star}',
                                                  style: TextStyle(
                                                    color: Colors.grey
                                                        .withOpacity(0.8),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            child: Center(
                                              child: Text(
                                                'Open Now',
                                                style: TextStyle(
                                                    color: Colors.green),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                                    Expanded(
                                        child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: (widget.shop.rating <=1 && widget.shop.review <=1 ) ? Text(
                                        '${widget.shop.rating} rating | ${widget.shop.review} Review',
                                        style: TextStyle(
                                            color:
                                                Colors.green.withOpacity(0.8)),
                                      ):Text(
                                        '${widget.shop.rating} ratings | ${widget.shop.review} Reviews',
                                        style: TextStyle(
                                            color:
                                            Colors.green.withOpacity(0.8)),
                                      ),
                                    )),
                                    Expanded(
                                        child: Container(
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.fastfood,
                                                  size: 18.0,
                                                  color: Colors.grey,
                                                ),
                                                SizedBox(
                                                  width: 5.0,
                                                ),
                                                Center(child: Text('${widget.shop.type}'))
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4.0),
                                            child: SizedBox(
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 2.0),
                                                width: 1.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),

                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.location_on,
                                                  size: 18.0,
                                                  color: Colors.grey,
                                                ),
                                                SizedBox(
                                                  width: 5.0,
                                                ),
                                                Text('${widget.shop.city}')
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                  ],
                                ),
                              ))
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
            SliverPersistentHeader(
              pinned: true,
              floating: false,
              delegate: _SliverPersistentHeader(tabBar: _tab),
            )
          ];
        },
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                      Icons.access_time,
                      color: Colors.red,
                    ),
                    title: Text('Hours Today: 7:00 am - 9:00 pm'),
                  ),
                  Divider(
                    indent: 10.0,
                    endIndent: 10.0,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    onTap: () {
                      print('Call....');
                    },
                    leading: Icon(
                      Icons.phone,
                      color: Colors.red,
                    ),
                    title: Text('096 662 2227'),
                  ),
                  Divider(
                    indent: 10.0,
                    endIndent: 10.0,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.attach_money,
                      color: Colors.red,
                    ),
                    title: Text('\$2.0 - \$15.0'),
                  ),
                  Divider(
                    indent: 10.0,
                    endIndent: 10.0,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.restaurant,
                      color: Colors.red,
                    ),
                    title: Text('Khmer | Coffee'),
                  ),
                  Divider(
                    indent: 10.0,
                    endIndent: 10.0,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.location_on,
                      color: Colors.red,
                    ),
                    title: Text('No 100, St 430, Chamkarnon, Phnom Penh'),
                  ),
                  Divider(
                    indent: 10.0,
                    endIndent: 10.0,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.red,
            ),
            Container(
              color: Colors.yellow,
            ),
            Container(
              color: Colors.green,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: CustomBottomAppBar(
          horizontal: true,
          items: [
            CustomBottomAppBarItem(
              onTab: () {
                print('Calling......');
                _launchURL();
              },
              icon: Icons.call,
              color: Colors.white,
              label: 'Call Now',
              labelStyle: TextStyle(color: Colors.white),
              backgroundColor: Colors.black87,
            ),
            CustomBottomAppBarItem(
              onTab: () {
                print('Map......');
                _mapRout(widget.shop.location);
              },
              icon: Icons.location_on,
              color: Colors.red,
              labelStyle: TextStyle(color: Colors.red),
              label: 'Map',
            )
          ],
        ),
      ),
    );
  }
}

class _SliverPersistentHeader extends SliverPersistentHeaderDelegate {
  final TabBar tabBar;

  _SliverPersistentHeader({this.tabBar});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      children: <Widget>[
        Material(color: Colors.white, elevation: 5.0, child: tabBar)
      ],
    );
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => tabBar.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }
}
