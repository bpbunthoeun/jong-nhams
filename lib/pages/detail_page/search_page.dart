import 'package:flutter/material.dart';
import 'package:jong_nhams/layout/card_layout.dart';
import 'package:jong_nhams/model/shop_model.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController textEditingController = TextEditingController();

  List<ShopModel> allShop;
  List<ShopModel> searchResult = [];

  @override
  void initState() {
    allShop = [
      ShopModel(
          id: 01,
          label: 'Coffee Culture',
          rating: 45,
          review: 56,
          imageUrl: 'assets/images/coffee_culture.png',
          phoneNumber: '096 662 2227',
          price: '\$2.0 - \$15.0',
          type: 'Coffee',
          city: 'Phnom Penh',
          star: 4.0,
          view: 350,
          location: '11.5399083,104.9102096'),
      ShopModel(
          id: 02,
          label: 'Coffee Other',
          rating: 12,
          review: 36,
          star: 3.0,
          imageUrl: 'assets/images/home_food_02.jpg',
          phoneNumber: '096 555 2227',
          price: '\$2.0 - \$15.0',
          type: 'Food',
          city: 'Phnom Penh',
          view: 230,
          location: '11.5399083,104.9102096'),
    ];

    super.initState();
  }

  onSearchTextChanged(String text) async {
    searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }
    allShop.forEach((shop) {
      if (shop.label.toUpperCase().contains(text.toUpperCase()))
        searchResult.add(shop);
      print('length: ${searchResult.length}');
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: Column(
            children: <Widget>[
              Container(
                width: _width,
                height: 93.0,
                decoration: BoxDecoration(color: Color(0XFFbd1e1f)),
                //color: Colors.red,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 33.0,
                    ),
                    Container(
                      height: 60.0,
                      child: Material(
                        color: Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  radius: 50.0,
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                      height: 60.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.arrow_back,
                                        color: Colors.white,
                                      )),
                                )),
                            Expanded(
                                flex: 6,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 0.0, vertical: 9.0),
                                  child: Material(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),

                                    //todo fix input text to center vertical
                                    child: TextField(
                                      controller: textEditingController,
                                      onChanged: onSearchTextChanged,
                                      maxLines: 1,
                                      textAlign: TextAlign.start,
                                      autofocus: true,
                                      decoration: InputDecoration(
                                        labelText: '',
                                          contentPadding:
                                              EdgeInsets.only(left: 15.0),
                                          border: InputBorder.none,
                                          hintText: 'Search',
                                          prefixIcon: Icon(Icons.search),
                                          hintStyle: TextStyle()),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: () {},
                                  child: Container(
                                      height: 60.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.map,
                                        color: Colors.white,
                                      )),
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 45.0,
                color: Colors.black26,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    FlatButton.icon(
                        //todo implement with bottom sheet
                        onPressed: () {},
                        icon: Icon(Icons.sort),
                        label: Text('Filter')),
                    FlatButton(onPressed: () {}, child: Text('Ranking')),
                    FlatButton(onPressed: () {}, child: Text('View')),
                    FlatButton(onPressed: () {}, child: Text('Proce')),
                  ],
                ),
              ),
              Visibility(
                visible: textEditingController.text.isEmpty ? false : true,
                child: Container(
                  padding: EdgeInsets.only(left: 18.0, top: 18.0),
                  alignment: Alignment.centerLeft,
                  height: 40.0,
                  child: (searchResult.length > 1)
                      ? Text(
                          '${searchResult.length} restaurants found',
                          style: TextStyle(fontSize: 18.0),
                        )
                      : Text(
                          '${searchResult.length} restaurant found',
                          style: TextStyle(fontSize: 18.0),
                        ),
                ),
              ),
              Expanded(
                child: textEditingController.text.isEmpty
                    ? Container()
                    : Container(
                        height: _height,
                        child: ListView.builder(
                            padding: EdgeInsets.symmetric(
                                horizontal: 6.0, vertical: 6.0),
                            itemCount: searchResult.length,
                            itemBuilder: (context, index) => CardLayout(
                                  shop: searchResult[index],
                                )),
                      ),
              )
            ],
          ),
        ),
      ],
    ));
  }
}
