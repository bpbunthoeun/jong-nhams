import 'package:flutter/material.dart';
import 'package:jong_nhams/model/shop_model.dart';
import 'package:jong_nhams/pages/detail_page/shop_detail_page.dart';

class CardLayout extends StatelessWidget {
  final ShopModel shop;

  CardLayout({this.shop});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 0.5, color: Colors.grey))),
      height: 130.0,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.only(
            top: 18.0, right: 6.0, left: 6.0, bottom: 12.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ShopDetailPage(
                          shop: shop,
                        )));
          },
          child: Material(
            color: Colors.transparent,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Row(
              children: <Widget>[
                Container(
                  //padding: EdgeInsets.all(8.0),
                  height: 150,
                  width: 150,
                  child: Image.asset(
                    '${shop.imageUrl}',
                    fit: BoxFit.contain,
                    filterQuality: FilterQuality.medium,
                  ),
                ),
                Expanded(
                    flex: 6,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                '${shop.label}',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Container(
                                padding: EdgeInsets.all(3.0),
                                height: 23.0,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(5.0)),
                                child: Center(
                                  child: Text(
                                    '0.2 km',
                                  ),
                                ),
                              )
                            ],
                          )),
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.yellow.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 1.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.yellow.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 1.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.yellow.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 1.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.yellow.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 1.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.grey.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 1.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                      color: Colors.grey.withOpacity(0.8),
                                    ),
                                    SizedBox(
                                      width: 2.0,
                                    ),
                                    Center(
                                      child: Text(
                                        '${shop.star}',
                                        style: TextStyle(),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 3.0, right: 3.0),
                                child: SizedBox(
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 2.0),
                                    width: 1.0,
                                    height: 20.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.visibility,
                                      size: 15,
                                      color: Colors.grey,
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                    Text('${shop.view} Views')
                                  ],
                                ),
                              )
                            ],
                          )),
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                  child: Text(
                                '${shop.rating} Ratings',
                                style: TextStyle(color: Colors.grey),
                              )),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 3.0, right: 3.0),
                                child: SizedBox(
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 2.0),
                                    width: 1.0,
                                    height: 20.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Container(
                                  child: Text(
                                '${shop.review} Reviews',
                                style: TextStyle(color: Colors.grey),
                              ))
                            ],
                          )),
                          Expanded(
                              child: Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.fastfood,
                                        size: 18.0,
                                        color: Colors.grey,
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Center(child: Text('${shop.type}'))
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 4.0),
                                  child: SizedBox(
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 2.0),
                                      width: 1.0,
                                      height: 20.0,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.location_on,
                                        size: 18.0,
                                        color: Colors.grey,
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Text('${shop.city}')
                                    ],
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  'Open now',
                                  style: TextStyle(color: Colors.green),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
