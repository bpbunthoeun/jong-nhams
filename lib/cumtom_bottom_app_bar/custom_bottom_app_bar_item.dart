import 'package:flutter/material.dart';

class CustomBottomAppBarItem extends StatelessWidget{
  final Function onTab;
  final IconData icon;
  final String label;
  final TextStyle labelStyle;
  final Color color;
  final Color backgroundColor;

  CustomBottomAppBarItem({Key key, this.onTab, this.icon, this.label, this.labelStyle, this.color, this.backgroundColor}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return null;
  }
}
