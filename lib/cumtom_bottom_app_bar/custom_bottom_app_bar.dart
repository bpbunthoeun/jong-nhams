import 'package:flutter/material.dart';

import 'custom_bottom_app_bar_item.dart';

class CustomBottomAppBar extends StatefulWidget {
  final List<CustomBottomAppBarItem> items;
  final bool horizontal;
  final Function onTab;

  CustomBottomAppBar({Key key, this.items, this.horizontal = false, this.onTab})
      : super(key: key);

  @override
  _CustomBottomAppBarState createState() => _CustomBottomAppBarState();
}

class _CustomBottomAppBarState extends State<CustomBottomAppBar>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  int selectedAppBar = 0;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200))
          ..forward();
    _animation =
        Tween<double>(begin: 0.8, end: 1.0).animate(_animationController);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  _playAnimation() {
    _animationController.reset();
    _animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _itemsList = List.generate(widget.items.length, (int index) {
      bool isSelected = selectedAppBar == index;
      return _buildItem(
          item: widget.items[index],
          index: index,
          isSelected: isSelected,
          horizontal: widget.horizontal);
    });
    return Container(
      height: 60.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: _itemsList,
      ),
    );
  }

  Widget _buildItem(
      {CustomBottomAppBarItem item,
      int index,
      bool isSelected,
      bool horizontal}) {
    return horizontal
        ? Expanded(
            child: Material(
            color: item.backgroundColor,
            //clipBehavior: Clip.antiAlias,
            //shape: StadiumBorder(),
            child: InkWell(
              onTap: () {
                item.onTab();
              },
              child: Container(
                height: 60.0,
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      item.icon,
                      color: item.color,
                      size: 28.0,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "${item.label}",
                      style: item.labelStyle,
                    )
                  ],
                ),
              ),
            ),
          ))
        : Expanded(
            child: Material(
            color: Colors.transparent,
            clipBehavior: Clip.antiAlias,
            shape: StadiumBorder(),
            child: InkWell(
              onTap: () {
                setState(() {
                  _playAnimation();
                  selectedAppBar = index;
                  widget.onTab(selectedAppBar);
                });
                print('index: $index');
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      isSelected
                          ? ScaleTransition(
                              scale: _animation,
                              child: Icon(
                                item.icon,
                                color: Color(0XFFbd1e1f),
                                size: 28.0,
                              ))
                          : Icon(
                              item.icon,
                              color: Colors.grey,
                              size: 24.0,
                            ),
                      SizedBox(
                        height: 3.0,
                      ),
                      AnimatedSize(
                        vsync: this,
                        curve: Curves.bounceInOut,
                        duration: Duration(milliseconds: 200),
                        child: Text(
                          "${item.label}",
                          style: isSelected
                              ? TextStyle(
                                  color: Color(0XFFbd1e1f), fontSize: 14)
                              : TextStyle(color: Colors.grey, fontSize: 12),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ));
  }
}
